import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

coffeebar = pd.read_csv("Coffeebar_2013-2017.csv", sep=";").fillna("nothing")

#PART 1
#Q1 Food & Drinks List
#Méthode  : Via la variable

foodList = coffeebar.groupby("FOOD").describe()
drinksList = coffeebar.groupby("DRINKS").describe()

uniqueCusto = coffeebar.groupby("CUSTOMER").nunique().count()
print("Number of different clients : ", uniqueCusto['CUSTOMER'])


#Q2 Total Food & Drinks Sold + Plot

totalFood = 0
for i, row in coffeebar.iterrows():
    if row['FOOD'] !='nothing':
        totalFood +=1
totalDrinks = coffeebar.DRINKS.count()

x = np.arange(2)
plt.bar(x,[totalFood,totalDrinks])
plt.xticks(x,['Food','Drinks'])
plt.title('Total amount of sold food and drinks')
plt.ylabel('Amount')
plt.show()

#Q3 probabilities

probaDico = {   'drinks': {'frappucino': 0, 'soda': 0, 'coffee': 0, 'tea': 0, 'water': 0, 'milkshake': 0},
                'food': {'nothing': 0, 'sandwich': 0, 'pie': 0, 'muffin': 0, 'cookie': 0}}
#######################################
#        Loading Product Dico         #
#######################################

def loadProductDic():
    dico = {}

    for i, row in coffeebar.iterrows():
        time = str(row[0].split(' ')[1])
        if time not in dico:
            dico[time] = {
                'drinks': {'frappucino': 0, 'soda': 0, 'coffee': 0, 'tea': 0, 'water': 0, 'milkshake': 0},
                'food': {'nothing': 0, 'sandwich': 0, 'pie': 0, 'muffin': 0, 'cookie': 0}}
        if row[2] in dico[time]['drinks']:
            dico[time]['drinks'][row[2]] += 1
        if row[3] in dico[time]['food']:
            dico[time]['food'][row[3]] += 1


    return dico

#######################################
#   Proba of each drink @Given Time   #
#######################################
#######################################
#   Proba of each food @Given Time   #
#######################################

def proba(hour, dico):

    global probaDico
    totalDrinkSold = dico[hour]['drinks']['frappucino'] + dico[hour]['drinks']['soda'] + \
                     dico[hour]['drinks']['coffee'] + dico[hour]['drinks']['tea'] + \
                     dico[hour]['drinks']['water'] + dico[hour]['drinks']['milkshake']

    probaFrappu = dico[hour]['drinks']['frappucino'] / totalDrinkSold * 100
    probaSoda = dico[hour]['drinks']['soda'] / totalDrinkSold * 100
    probaCoffee = dico[hour]['drinks']['coffee'] / totalDrinkSold * 100
    probaWater = dico[hour]['drinks']['water'] / totalDrinkSold * 100
    probaTea = dico[hour]['drinks']['tea'] / totalDrinkSold * 100
    probaMilkshake = dico[hour]['drinks']['milkshake'] / totalDrinkSold * 100

    probaDico['drinks']['frappucino'] = probaFrappu
    probaDico['drinks']['soda'] = probaSoda
    probaDico['drinks']['coffee'] = probaCoffee
    probaDico['drinks']['water'] = probaWater
    probaDico['drinks']['tea'] = probaTea
    probaDico['drinks']['milkshake'] = probaMilkshake

    #print("On average the probability of a customer at ", hour,
          #"buying frappucino, soda, coffee, tea, water and milkshake is",
          #probaFrappu, probaSoda, probaCoffee, probaTea, probaWater, probaMilkshake)

    totalFoodSold = dico[hour]['food']['sandwich'] + dico[hour]['food']['pie'] + \
                    dico[hour]['food']['muffin'] + dico[hour]['food']['cookie'] + \
                    dico[hour]['food']['nothing']

    if(totalFoodSold != 0):
        probaSandwich = dico[hour]['food']['sandwich'] / totalFoodSold * 100
        probaPie = dico[hour]['food']['pie'] / totalFoodSold * 100
        probaMuffin = dico[hour]['food']['muffin'] / totalFoodSold * 100
        probaCookie = dico[hour]['food']['cookie'] / totalFoodSold * 100
        probaNothing = dico[hour]['food']['nothing'] / totalFoodSold * 100

        probaDico['food']['sandwich'] = probaSandwich
        probaDico['food']['pie'] = probaPie
        probaDico['food']['muffin'] = probaMuffin
        probaDico['food']['cookie'] = probaCookie
        probaDico['food']['nothing'] = probaNothing

        #print("On average the probability of a customer at ", hour,
          #"buying sandwich, pie, muffin, cookie, nothing is",
          #probaSandwich, probaPie, probaMuffin, probaCookie, probaNothing)
    else:
        print("No food sold @that time")

    return probaDico

sellDic = {}

# PART 4
#Q1
#######################################
#        Returning Custo Stats        #
#######################################

coffeebar['is_duplicated'] = coffeebar.duplicated(['CUSTOMER'])

duplicList = []

for i, row in coffeebar.iterrows():
    if row['is_duplicated'] and row['CUSTOMER'] not in duplicList:
        duplicList.append(row['CUSTOMER'])

print("Total of returning Customer : ", len(duplicList))


