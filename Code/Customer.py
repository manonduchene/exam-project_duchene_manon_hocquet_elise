import random
import datetime
#import matplotlib.pyplot as plt
import numpy as np
from Exploratory import loadProductDic
from Exploratory import proba

#PART 2 - PART 3
numberOfReturning = 1000
#Change this for PART 4 - Q3
#numberOfReturning = 50
hispterPc = 33
tripPc = 10
hipsterNb = numberOfReturning * hispterPc / 100
nextID = 10000000
returningPc = 20.0
sellDico = {}
probaDico = {}
nbDay = 364*5
startTime = datetime.datetime(2013, 1, 1, 8, 0, 0)
endTime = datetime.datetime(2017, 12, 31, 17, 55, 0)
#For PART 4 - Q4 : +20% price
#Uncomment this two commands
#upPriceTime = 2015
#priceChange = 1.20
totalSell = 0
totalTip = 0


priceDico = {'drinks': {'frappucino': 4, 'soda': 3, 'coffee': 0, 'tea': 3, 'water': 2, 'milkshake': 5},
                'food': {'': 0, 'sandwich': 5, 'pie': 3, 'muffin': 3, 'cookie': 2}}

#Types of clients : oneRegular - oneTripadvisor - returningReguler - returningHipster

#######################################
#      Creating Object Customer       #
#######################################

class Customer:

    def __init__(self, type):
        global nextID
        global returningPool

        self.type = type
        self.dico = {}

        if self.type == "oneRegular" or self.type == "oneTripadvisor":
            self.budget = 100
            if self.type == "oneTripadvisor":
                self.tip = random.randint(1,10)
        elif self.type == "returningRegular":
            self.budget = 250
        elif self.type == "returningHipster":
            self.budget = 500
            #Change this for PART 4 - Q5
            #self.budget = 40

        self.customerID = "CID"+ str(nextID)
        nextID += 1

    #######################################
    #     Method to add drink & food      #
    #######################################

    def addProduct(self, time, year, food, drink, id=0):
        global totalSell
        global totalTip

        self.dico[time] = {
                'drinks': {'frappucino': 0, 'soda': 0, 'coffee': 0, 'tea': 0, 'water': 0, 'milkshake': 0},
                'food': {'': 0, 'sandwich': 0, 'pie': 0, 'muffin': 0, 'cookie': 0}}

        if drink in self.dico[time]['drinks']:
            self.dico[time]['drinks'][drink] += 1
        if food in self.dico[time]['food']:
            self.dico[time]['food'][food] += 1


        if food in priceDico['food']:
            price = priceDico['food'][food]
            # PART 4 PRICE +20%
            #if year >= upPriceTime:
            #    price += priceDico['food'][food] * priceChange
            #   self.budget -= price
            #else:
            #    self.budget -= price
            self.budget -= price
            totalSell += price

        if drink in priceDico['drinks']:
            price = priceDico['drinks'][drink]
            # PART 4 PRICE +20%
            #if year >= upPriceTime:
            #    price += priceDico['drinks'][drink] * priceChange
            #    self.budget -= price
            #else:
            #    self.budget -= price
            self.budget -= price
            totalSell += price

# PART 4 - Q4
        #It is based from the previous step
        #The line 24-25 must be uncommented
        if food in priceDico['food']:
            price = priceDico['food'][food]
            # PART 4 PRICE +20%
            if year >= upPriceTime:
                price += priceDico['food'][food] * priceChange
                self.budget -= price
            else:
                self.budget -= price
            # self.budget -= price
            totalSell += price

        if drink in priceDico['drinks']:
            price = priceDico['drinks'][drink]
            # PART 4 PRICE +20%
            if year >= upPriceTime:
                price += priceDico['drinks'][drink] * priceChange
                self.budget -= price
            else:
                self.budget -= price
            # self.budget -= price
            totalSell += price


        if self.type == "oneTripadvisor":
            self.budget -= self.tip
            totalTip += self.tip

        if self.budget < 10:
        #PART 4 +20%
        #if self.budget < (10 + (10*priceChange)):
            returningPool.remove(self.customerID)

#######################################
#    Function to define food drink    #
#######################################

def whatToBuy(probaDico):
    tmpRdm = random.randint(0,100)
    buy = []
#Drink proba
    proba1 =  probaDico['drinks']['frappucino']+probaDico['drinks']['soda']
    proba2 =  probaDico['drinks']['frappucino']+probaDico['drinks']['soda']+probaDico['drinks']['coffee']
    proba3 =  probaDico['drinks']['frappucino']+probaDico['drinks']['soda']+probaDico['drinks']['coffee']+probaDico['drinks']['water']
    proba4 =  probaDico['drinks']['frappucino']+probaDico['drinks']['soda']+probaDico['drinks']['coffee']+probaDico['drinks']['water']+probaDico['drinks']['tea']

    if tmpRdm <= probaDico['drinks']['frappucino']:
        buy.append("frappucino")
    elif tmpRdm > probaDico['drinks']['frappucino'] and tmpRdm <= proba1:
        buy.append("soda")
    elif tmpRdm > proba1 and tmpRdm <= proba2:
        buy.append("coffee")
    elif tmpRdm > proba2 and tmpRdm <= proba3:
        buy.append("water")
    elif tmpRdm > proba3 and tmpRdm <= proba4:
        buy.append("tea")
    elif tmpRdm > proba4:
        buy.append("milkshake")

#Food proba

    tmpRdm = random.randint(0,100)

    proba1 =  probaDico['food']['sandwich']+probaDico['food']['pie']
    proba2 =  probaDico['food']['sandwich']+probaDico['food']['pie']+probaDico['food']['muffin']
    proba3 =  probaDico['food']['sandwich']+probaDico['food']['pie']+probaDico['food']['muffin']+probaDico['food']['cookie']

    if tmpRdm <= probaDico['food']['sandwich']:
        buy.append("sandwich")
    elif tmpRdm > probaDico['food']['sandwich'] and tmpRdm <= proba1:
        buy.append("pie")
    elif tmpRdm > proba1 and tmpRdm <= proba2:
        buy.append("muffin")
    elif tmpRdm > proba2 and tmpRdm <= proba3:
        buy.append("cookie")
    elif tmpRdm > proba3:
        buy.append("nothing")
    return buy

#######################################
#       Simulation Preparation        #
#######################################

#######################################
#     List of Returning Customer      #
#######################################

returningList = []
returningPool = []
newCustolist = []

for i in range(0, numberOfReturning):
    if i < hipsterNb:
        returningList.append(Customer("returningHipster"))
    else:
        returningList.append(Customer("returningRegular"))

    returningPool.append(returningList[i].customerID)

#Recover probabilities food & drink

sellDico = loadProductDic()


#Random to know if returning or new custo

i = 0
nextTime = startTime

while(nextTime < endTime):

    probaDico = proba(str(nextTime.time()), sellDico)
    purchase = whatToBuy(probaDico)

    tmpRdm = random.randint(0, 100)

    if tmpRdm < returningPc:
        #Take to the pool
        if returningPool:
            tmpRdm = random.randint(0, len(returningPool)-1)
            idReturn = returningPool[tmpRdm]

            for i in range(0, len(returningPool)):
                if idReturn == returningList[i].customerID:
                    returningList[i].addProduct(nextTime.time(),nextTime.year, purchase[1], purchase[0], i)
                    #ADD FOOD
        else:
            print("NO More Returning")
    elif tmpRdm >= returningPc:
        tmpRdm = random.randint(0, 100)
        # TripAdvisor Customer
        if (tmpRdm < tripPc):
            newCustolist.append(Customer("oneTripadvisor"))
        # Regular
        else:
            newCustolist.append(Customer("oneRegular"))


        newCustolist[-1].addProduct(nextTime.time(),nextTime.year, purchase[1], purchase[0])


    if nextTime.hour < 11:
        nextTime = nextTime + datetime.timedelta(0, 300)
    elif nextTime.hour < 13 and nextTime.hour >= 11:
        nextTime = nextTime + datetime.timedelta(0, 120)
    elif nextTime.hour >= 13:
        nextTime = nextTime + datetime.timedelta(0, 240)

    if nextTime.hour == 18:
        nextTime = nextTime + datetime.timedelta(1, 0)
        nextTime = nextTime.replace(hour=8)

averageIncome = totalSell / nbDay

x = np.arange(2)
plt.bar(x, [totalTip, totalSell])
plt.xticks(x, ['Total Tip', 'Total Sold'])
plt.title('Sell and Tip')
plt.ylabel('Amount in \u20AC')
plt.show()

x = np.arange(1)
plt.bar(x, [averageIncome])
plt.xticks(x, ['Average per day'])
plt.title('Average Income')
plt.ylabel('Amount in \u20AC')
plt.show()

#PART 4
#Q2

for i in range(0, 10):
    rdm = random.randint(0, len(returningList)-1)
    print("Historique de : ", returningList[rdm].customerID)
    print(returningList[rdm].dico)